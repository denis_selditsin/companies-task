<?php


namespace Kernel;


interface Model
{
    public function main(array $post_args = null, string $get_args = null);
}