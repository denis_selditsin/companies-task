<?php


namespace Kernel;

use Exception;
use Kernel\View;
use Kernel\Model;

abstract class Controller
{
    public Model $model;
    public View $view;

    function __construct()
    {
        $this->view = new View();

        $model_name = "Model_".explode("_", get_class($this))[1];
        if(class_exists($model_name))
            $this->model = new $model_name;
    }

    abstract function action_main(array $post_args = null, string $get_args = null);

}