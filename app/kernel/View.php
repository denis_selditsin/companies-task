<?php


namespace Kernel;


class View
{

    public function generate($content_view, $template_view, $result = null)
    {
        require_once "app/views/".$template_view;
    }

}