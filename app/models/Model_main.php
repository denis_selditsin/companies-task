<?php

use Entities\Company;
use Kernel\Model;
use Kernel\DBConnector;

class Model_main implements Model
{

    private static array $fields = ["id", "name", "inn", "general_information", "general_manager", "address", "phone"];


    public function main(array $post_args = null, string $get_args = null)
    {
        $ids = DBConnector::query("SELECT id FROM companies")->fetchAll(PDO::FETCH_ASSOC);
        $companies = [];
        foreach ($ids as $id) {
            $company = new Company($id['id']);
            $companies[] = $company->get_all();
        }
        return $companies;
    }

    public function add(array $post_args = null, string $get_args = null)
    {
        if ($this->validate_company($post_args)){
            $query = DBConnector::query("INSERT INTO companies VALUES (0, '".$post_args['name']."', '".$post_args['inn']."',
        '".$post_args['general_information']."', '".$post_args['general_manager']."', '".$post_args['address']."',
        '".$post_args['phone']."')");
            if($query !== false)
                return array("result" => true);
            else
                return array("result" => false);
        }
        else
            return array("result" => false);
    }

    private function validate_company($post_args): bool
    {
        if (array_key_exists("name", $post_args) && !empty($post_args['name']) &&
            array_key_exists("inn", $post_args) && !empty($post_args['inn']) &&
            array_key_exists("general_information", $post_args) && !empty($post_args['general_information']) &&
            array_key_exists("general_manager", $post_args) && !empty($post_args['general_manager']) &&
            array_key_exists("address", $post_args) && !empty($post_args['address']) &&
            array_key_exists("phone", $post_args) && !empty($post_args['phone']))
            return true;
        else
            return false;
    }




}