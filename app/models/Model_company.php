<?php


use Entities\Comments;
use Entities\Company;
use Kernel\Model;

class Model_company implements Model
{

    public function main(array $post_args = null, string $get_args = null)
    {
        $company = new Company((int)$get_args);
        return $company->get_all();
    }

    public function get_comments(array $post_args = null, string $get_args = null)
    {
        if(array_key_exists("id", $post_args) && array_key_exists("field", $post_args))
        {
            $id = (int)$post_args["id"];
            $field = $post_args["field"];
            $company = new Company($id);
            return $company->get_comments($field);
        }
    }

    public function add_comment(array $post_args = null, string $get_args = null)
    {
        if($this->validate_comment($post_args)){
            $id = (int)$post_args["id"];
            $field = $post_args["field"];
            $comment = $post_args["text"];
            $company = new Company($id);


            $query = $company->add_comment($field, $comment);
            if($query !== false)
                return array("result" => true);
            else
                return array("result" => false);
        }else return array("result" => false);

    }

    private function validate_comment($post_args){
        if (array_key_exists("id", $post_args) && !empty($post_args['id']) &&
            array_key_exists("field", $post_args) && !empty($post_args['field']) &&
            array_key_exists("text", $post_args) && !empty($post_args['text']))
            return true;
        else
            return false;
    }


}