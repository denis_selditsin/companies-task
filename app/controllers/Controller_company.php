<?php


use Kernel\Controller;

class Controller_company extends Controller
{

    function action_main(array $post_args = null, string $get_args = null)
    {
        $result = $this->model->main($post_args, $get_args);
        $this->view->generate('view_company.php', 'template_page.php', $result);
    }

    public function action_get_comments(array $post_args = null, string $get_args = null){
        $result = $this->model->get_comments($post_args, $get_args);
        $this->view->generate('view_json.php', 'template_json.php', $result);
    }

    public function action_add_comment(array $post_args = null, string $get_args = null){
        $result = $this->model->add_comment($post_args, $get_args);
        $this->view->generate('view_json.php', 'template_json.php', $result);
    }

}