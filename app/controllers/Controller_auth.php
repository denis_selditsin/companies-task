<?php


use Kernel\Controller;

class Controller_auth extends Controller
{

    function action_main(array $post_args = null, string $get_args = null)
    {
        $result = $this->model->main($post_args, $get_args);
        if(array_key_exists("username", $_SESSION) && !empty($_SESSION["username"])){
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:'.$host);
        }
        $this->view->generate('view_auth.php', 'template_page.php', $result);
    }
}