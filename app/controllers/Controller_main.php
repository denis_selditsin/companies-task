<?php

use Kernel\Controller;
use Kernel\View;

class Controller_main extends Controller
{
    public function action_main(array $post_args = null, string $get_args = null)
    {
        $result = $this->model->main($post_args, $get_args);
        $this->view->generate('view_main.php', 'template_page.php', $result);
    }

    public function action_add(array $post_args = null, string $get_args = null)
    {
        $result = $this->model->add($post_args, $get_args);
        $this->view->generate('view_json.php', 'template_json.php', $result);
    }

    public function action_get(array $post_args = null, string $get_args = null)
    {
        $result = $this->model->main($post_args, $get_args);
        $this->view->generate('view_json.php', 'template_json.php', $result);
    }
}
