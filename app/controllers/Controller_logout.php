<?php


use Kernel\Controller;

class Controller_logout extends Controller
{

    function action_main(array $post_args = null, string $get_args = null)
    {
        session_destroy();
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:'.$host.'auth');
    }
}