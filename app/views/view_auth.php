<h3 class="mt-2">Сперва необходимо авторизоваться</h3>
<div>
    <div class="mx-auto col-md-6 mt-4 border p-4">
        <form action="/auth" method="post">
            <div class="form-group">
                <input type="text" placeholder="Введите имя пользователя" class="form-control" name="username">
            </div>
            <button type="submit" class="btn btn-primary col-12">Войти</button>
        </form>
    </div>

</div>
