<div class="row pt-1" id="companies">
</div>
<div class="row">
    <p class="m-1">
        <button class="btn btn-primary mt-4" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Добавить компанию
        </button>
    </p>
</div>
<p id="test"></p>
<div class="row">
    <div class="collapse col-lg-9 col-md-12 p-0 m-1 pr-2" id="collapseExample">
        <div class="card card-body">
            <div class="row">
                <div class="alert alert-success m-3 text-center" role="alert" id="success" style="display: none">
                    Компания успешно добавлена!
                </div>
                <div class="alert alert-danger m-3 text-center" role="alert" id="error" style="display: none">
                    Ошибка. Возможно не все поля заполнены, или заполнены некорректно.
                </div>
            </div>
            <form class="mt-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" placeholder="Название компании">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inn" placeholder="ИНН">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="general_information" placeholder="Общая информация">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="general_manager" placeholder="Генеральный директор">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="address" placeholder="Адрес">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="phone" placeholder="Телефон">
                </div>
                <button id="add" type="button" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        let shown_companies = [];
        get_companies();
        $("#add").click(function () {
            $("#success").hide();
            $("#error").hide();
            let name = $("#name").val();
            let inn = $("#inn").val();
            let general_information = $("#general_information").val();
            let general_manager = $("#general_manager").val();
            let address = $("#address").val();
            let phone = $("#phone").val();

            $.ajax({
                type: "POST",
                url: "main/add",
                data: { name: name,
                    inn: inn,
                    general_information: general_information,
                    general_manager: general_manager,
                    address: address,
                    phone: phone },
                dataType: "json"
            }).done(function( result )
            {
                console.log(result);
                if (result.result === true)
                    $("#success").show();
                else
                    $("#error").show();
                get_companies();
            });

        });
        function get_companies() {
            $.ajax({
                type: "POST",
                url: "main/get",
                dataType: "json"
            }).done(function( result )
            {
                result.forEach(function (company) {
                    if(shown_companies.indexOf(company.id) === -1){
                        shown_companies.push(company.id)
                        $("#companies").append("<div class=\"col-md-3 col-sm-6 my-1 px-1 company\"> \n" +
                            "                <div class=\"card\">\n" +
                            "                    <div class=\"card-body\">\n" +
                            "                        <a href=\"/company/main/"+company.id+"\" class=\"stretched-link\">  <h5 class=\"card-title\">"+company.name+"</h5></a>\n" +
                            "                        <p class=\"card-text\">"+company.general_information+"</p>\n" +
                            "                    </div>\n" +
                            "                </div>\n" +
                            "            </div>");
                    }
                });
            });
        }
    });


</script>



