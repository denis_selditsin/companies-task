
<div class="row my-2 px-2">
    <?php
        foreach (  array_keys($result) as $result_key) {
            if ($result_key!="id" && $result_key!="ru") {
                echo "
                    <div class=\"card col-12 my-2\">
                        <div class=\"card-body row\">
                            <div class=\"col-6 justify-content-center align-self-center\">
                                <h5>" . $result[$result_key] . "</h5><span class=\"font-weight-light\">" . $result["ru"][$result_key] . "</span>
                            </div>
                            <div class=\"col-6 text-right justify-content-center align-self-center\">
                                <a data-toggle=\"collapse\" href=\"#comments-$result_key\" role=\"button\" aria-expanded=\"false\" aria-controls=\"comments-$result_key\">+ -</a> Комментарии <span id='count-$result_key'></span>
                            </div>
                        </div>
                    </div>
                    <div class=\"collapse col-md-10 p-0 mb-3\" id=\"comments-$result_key\">
                        <div class=\"card card-body mb-2\">
                            <form>
                                <div class=\"form-group\">
                                    <label for=\"comment-for-$result_key\">Комментарий</label>
                                    <textarea class=\"form-control\" id=\"comment-for-$result_key\" name=\"text\" rows=\"3\" required></textarea>
                                </div>
                                <button type=\"button\" class=\"btn btn-primary\" id=\"$result_key\">Добавить</button>
                                <div class=\"row\">
                                    <div class=\"alert alert-success m-3 text-center\" role=\"alert\" id=\"success-$result_key\" style=\"display: none\">
                                        Комментарий успешно добавлен!
                                    </div>
                                    <div class=\"alert alert-danger m-3 text-center\" role=\"alert\" id=\"error-$result_key\" style=\"display: none\">
                                        Ошибка. Заполните текстовое поле.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"card card-body col-md-10 mb-1 comments-list-$result_key\">
                        <h4>Комментарии</h4>
                           <div class=\"card my-2\" id=\"empty-$result_key\" style=\"display: none\">
                                <div class=\"card-body py-2\">
                                    <p class=\"card-text text-monospace text-muted mt-2\">Нет комментариев<p>
                                </div>
                            </div>
                        </div>
                ";
            }
        }
    ?>

    <div class="card col-12 my-2">
        <div class="card-body row">
            <div class="col-6 justify-content-center align-self-center">
                <h4>Комментарии к компании</h4>
            </div>
            <div class="col-6 text-right justify-content-center align-self-center">
                <a data-toggle="collapse" href="#comments-company" role="button" aria-expanded="false" aria-controls="comments-company">+ -</a> Комментарии <span id='count-company'></span>
            </div>
        </div>
    </div>
    <div class="collapse col-md-10 p-0 mb-3" id="comments-company">
        <div class="card card-body mb-2">
            <form>
                <div class="form-group">
                    <label for="comment-for-company">Комментарий</label>
                    <textarea class="form-control" id="comment-for-company" name="text" rows="3" required></textarea>
                </div>
                <button type="button" class="btn btn-primary" id="company">Добавить</button>
                <div class="row">
                    <div class="alert alert-success m-3 text-center" role="alert" id="success-company" style="display: none">
                    Комментарий успешно добавлен!
                </div>
                <div class="alert alert-danger m-3 text-center" role="alert" id="error-company" style="display: none">
                Ошибка. Заполните текстовое поле.
        </div>
    </div>
            </form>
        </div>
    </div>
    <div class="card card-body comments-list-company col-md-10">
        <h4>Комментарии</h4><span id='count-company'></span>
        <div class="card my-2" id="empty-company" style="display: block">
            <div class="card-body py-2">
                <p class="card-text text-monospace text-muted mt-2">Нет комментариев<p>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        let shown_comments = [];
        let fields = ["name",
                    "inn",
                    "general_information",
                    "general_manager",
                    "address",
                    "phone",
                    "company"];
        update_comments();
        $("button").click(function () {
            let field = this.getAttribute("id");
            let comment = $("#comment-for-"+field).val();
            $("#success-"+field).hide();
            $("#error-"+field).hide();

            $.ajax({
                type: "POST",
                url: "/company/add_comment",
                data: { id: <?php echo $result["id"]?>,
                        field: field,
                        text : comment},
                dataType: "json"
            }).done(function( result )
            {
                if(result.result === false)
                    $("#error-"+field).show();
                else{
                    $("#success-"+field).show();
                    setTimeout(function() {
                        $("#comments-" + field).collapse('hide');
                        $("#success-"+field).hide();
                        $("#comment-for-"+field).val("");
                    }, 500);
                }


                update_comments();
            });
        });

        function update_comments(){
            fields.forEach(function (field) {
                get_comments(field);
            });
        }


        function get_comments(field) {
            $.ajax({
                type: "POST",
                url: "/company/get_comments",
                data: { id: <?php echo $result["id"]?>,
                    field: field},
                dataType: "json"
            }).done(function( result )
            {
                $("#count-"+field).text(result.length);
                if (result.length === 0){
                    $("#empty-"+field).show();
                }else {
                    $("#empty-"+field).hide();
                }
                result.forEach(function (comment) {
                    if(shown_comments.indexOf(comment.id) === -1){
                        shown_comments.push(comment.id)
                        $("div.comments-list-"+field).append(
                            "        <div class=\"card my-2\">\n" +
                            "            <div class=\"card-body py-2\">\n" +
                            "               <span class=\"card-title font-weight-bold\">"+comment.username+"</span> <span class=\"card-subtitle mb-2 text-muted\">"+comment.time+"</span>\n" +
                            "               <p class=\"card-text text-monospace text-muted mt-2\">"+comment.text+"</p>\n" +
                            "            </div>\n" +
                            "        </div>");
                    }

                });
            });

        }

    });
</script>

