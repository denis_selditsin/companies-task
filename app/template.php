<?php
require_once __DIR__."/../vendor/autoload.php";
session_start();
use Kernel\Router;
use Kernel\Model;
use Kernel\View;
use Kernel\Controller;

Router::route();
