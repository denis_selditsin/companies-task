<?php

namespace Entities;
use Kernel\DBConnector;
use PDO;

class Company
{
    private string $id = "";
    private string $name = "";
    private string $inn = "";
    private string $general_information = "";
    private string $general_manager = "";
    private string $address = "";
    private string $phone = "";

    public function __construct(int $id)
    {
        $this->id = $id;
        $query = DBConnector::query("SELECT * FROM companies WHERE id=$this->id")->fetch();
        if ($query !== false){
            $this->name = $query["name"];
            $this->inn = $query["inn"];
            $this->general_information = $query["general_information"];
            $this->general_manager = $query["general_manager"];
            $this->address = $query["address"];
            $this->phone = $query["phone"];
            return true;
        }else
            return false;

    }

    public static function get_companies(array $post_args = null, string $get_args = null)
    {
        $ids = DBConnector::query("SELECT id FROM companies")->fetchAll(PDO::FETCH_ASSOC);
        $companies = [];
        foreach ($ids as $id) {
            $company = new Company($id['id']);
            $companies[] = $company->get_all();
        }
        return $companies;
    }

    public function get_all()
    {
        return ["id" => $this->get_id(),
                "name" => $this->get_name(),
                "inn" => $this->get_inn(),
                "general_information" => $this->get_general_information(),
                "general_manager" => $this->get_general_manager(),
                "address" => $this->get_address(),
                "phone" => $this->get_phone(),
                "ru" => ["name" => "Название компании",
                        "inn" => "ИНН",
                        "general_information" =>"Общая информация",
                        "general_manager" => "Генеральный директор",
                        "address" => "Адрес",
                        "phone" => "Телефон"]
        ];
    }


    public function get_id()
    {
        return $this->id;
    }
    public function get_name()
    {
        return $this->name;
    }
    public function get_inn()
    {
        return $this->inn;
    }

    public function get_general_information()
    {
        return $this->general_information;
    }

    public function get_general_manager()
    {
        return $this->general_manager;
    }

    public function get_address()
    {
        return $this->address;
    }

    public function get_phone()
    {
        return $this->phone;
    }

    public function get_comments($field){
        return DBConnector::query("SELECT * FROM comments WHERE company_id=".$this->get_id()." AND username='".$_SESSION["username"]."' AND field = '$field'")->fetchAll(PDO::FETCH_ASSOC);
    }

    public function add_comment($field, $comment){
        $datetime = date("Y-m-d H:i:s");
        return DBConnector::query("INSERT INTO comments VALUES (0, '".$this->get_id()."', '$field', '".$_SESSION["username"]."', '$comment', '$datetime')")->fetchAll(PDO::FETCH_ASSOC);
    }

}